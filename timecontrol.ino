////////////////////////////////////////////////////////////////////////////////
// TIMEControl
////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////
// Libraries used
////////////////////////////////////////////////////////////////////////////////

#define XCONTROL_NAME F("TIMEControl")     // application name
#define XCONTROL_VER  F("v1.0.1")          // application version

//#include <SPI.h>                         // Arduino standard Ethernet/SPI lib
//#include <Ethernet.h>
//#include <EthernetUdp.h>

#include <UIPEthernet.h>                   // UIP Ethernet library



////////////////////////////////////////////////////////////////////////////////
// Circuit setup
////////////////////////////////////////////////////////////////////////////////

// menu configuration
const byte menuCount PROGMEM     = 1;      // number of menu items
const String menuNames[]         = {       // menu display naming
  "Time control"
};
const String menuLinks[]         = {       // menu href links
  "http://192.168.15.55"
};

// network configuration
byte netMAC[]                    = {       // mac address
  0xDE, 0xAD, 0xB0,
  0xEF, 0xFD, 0x01
};
byte netIP[]                     = {       // ip address
  192, 168, 15, 55
};

unsigned int localPort = 8888;
char timeServer[] = "time.nist.gov";
const int NTP_PACKET_SIZE = 48;
byte packetBuffer[NTP_PACKET_SIZE];



////////////////////////////////////////////////////////////////////////////////
// Runtime variables
////////////////////////////////////////////////////////////////////////////////

//ethernet http server setup
EthernetServer netServer(80);

//ethernet ntp udp client setup
EthernetUDP netUdp;



////////////////////////////////////////////////////////////////////////////////
// Setup / main section
////////////////////////////////////////////////////////////////////////////////

//setup routine
void setup() {
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Ethernet.begin(netMAC, netIP);
  netServer.begin();
  netUdp.begin(localPort);
}

//main loop
void loop() {

  interruptHttp();
  delay(10);

/*  
  ntpProcess(timeServer);
  delay(1000);
  if (Udp.parsePacket()) {
    Udp.read(packetBuffer, NTP_PACKET_SIZE);
    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    unsigned long secsSince1900 = highWord << 16 | lowWord;
    Serial.print("Seconds since Jan 1 1900 = ");
    Serial.println(secsSince1900);

    Serial.print("Unix time = ");
    const unsigned long seventyYears = 2208988800UL;
    unsigned long epoch = secsSince1900 - seventyYears;
    Serial.println(epoch);

    Serial.print("The UTC time is ");
    Serial.print((epoch  % 86400L) / 3600);
    Serial.print(':');
    if (((epoch % 3600) / 60) < 10) {
      Serial.print('0');
    }
    Serial.print((epoch  % 3600) / 60);
    Serial.print(':');
    if ((epoch % 60) < 10) {
      Serial.print('0');
    }
    Serial.println(epoch % 60);
  }
  delay(10000);
  Ethernet.maintain();

*/
}



////////////////////////////////////////////////////////////////////////////////
// HTTP and NTP request processing
////////////////////////////////////////////////////////////////////////////////

// main http processing
void httpProcess(EthernetClient ec, String url) {
  String url2 = url;
  if (url == "" or url == "/") {
    htmlHeader(ec);
    htmlMenu(ec);
    htmlTime(ec);
    htmlFooter(ec);
  } else if (url.startsWith("/time")) {
//
  } else if (url.startsWith("/countdown")) {
//
  } else if (url.startsWith("/css")) {
    htmlCSS(ec);
  } else if (url.startsWith("/js/common")) {
    htmlJSCommon(ec);
  } else if (url.startsWith("/js/custom")) {
    htmlJSCustom(ec);
  }
}

// time display mode 
void htmlTime(EthernetClient ec) {
  ec.println(F("  <card>"));
  ec.print  (F("    <name>"));
  ec.print  ("23:45:58");
  ec.println(F("</name>"));
  ec.print(F("    <action>"));
  ec.print(F("This is the message somebody sent"));
  ec.println(F("</action>"));
  ec.println(F("  </card>"));

  ec.println(F("  <card>"));
  ec.print  (F("    <name>"));
  ec.print  ("Server: time.123.org");
  ec.println(F("</name>"));
  ec.println(F("    <action>"));
  ec.println(F("      synchronised: never"));
  ec.println(F("      <state class=\"active\"/>Synchronise</state>"));
  ec.println(F("    </action>"));
  ec.println(F("  </card>"));

  ec.println(F("  <card>"));
  ec.print  (F("    <name>"));
  ec.print  ("Time mode");
  ec.println(F("</name>"));
  ec.println(F("    <action>"));
  ec.println(F("      updated: never"));
  ec.println(F("      <state class=\"active\"/>Time</state>"));
  ec.println(F("      <state/>Countdown</state>"));
  ec.println(F("    </action>"));
  ec.println(F("  </card>"));
}

// countdown display mode
void htmlCountdown(EthernetClient ec) {
  
}

// js file
void htmlJSCustom(EthernetClient ec) {
  httpHeader(ec, "application/javascript");
  ec.println(F("function xcRelaySwitch(r, s) {"));
  ec.println(F("  var s2=(s=='on'?'off':'on');"));
  ec.println(F("  var el=xcFind('#state'+r+s2);"));
  ec.println(F("  xcRemoveClass(el,'active');"));
  ec.println(F("  xcJSON('/switch/'+s+'/'+r, function(){var el=xcFind('#state'+r+s);xcAddClass(el,'active');});"));
  ec.println(F("}\n"));
}

// main ntp processing
void ntpProcess(char* address) {
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;
  netUdp.beginPacket(address, 123);
  netUdp.write(packetBuffer, NTP_PACKET_SIZE);
  netUdp.endPacket();
}



////////////////////////////////////////////////////////////////////////////////
// Common HTTP and HTML functions for RELAY/LED/TEMP/TIME -Control sketches
////////////////////////////////////////////////////////////////////////////////

// handle basic request received with http
void interruptHttp() {
  boolean currentBlank;
  String currentLine;
  String currentURL;
  String currentReq;
  char c;
  EthernetClient client = netServer.available();
  if (client) {
    currentBlank = true;
    currentLine  = "";
    currentURL   = "";
    while (client.connected()) {
      if (client.available()) {
        c = client.read();
        if (c == '\n' && currentBlank) {
          httpProcess(client, currentURL);
          break;
        }
        if (c == '\n' or c == '\r') {
          currentBlank = true;
          if (currentLine.indexOf('GET') > 0) {
            currentURL = currentLine;
            currentURL.replace("GET", "");
            currentURL.replace("HTTP/1.1", "");
            currentURL.replace(" ", "");
            currentLine = "";
          }
        } else if (c != '\r') {
          currentBlank = false;
          currentLine.concat(c);
        }
      }
    }
    delay(50);
    client.stop();
  }
}

// http response header
void httpHeader(EthernetClient ec, String ct) {
  ec.println(F("HTTP/1.1 200 OK"));
  ec.print  (F("Content-Type: "));
  ec.println(ct);
  ec.println(F("Connection: close"));
  ec.println();
}

// html header fields and start html body
void htmlHeader(EthernetClient ec) {
  httpHeader(ec, "text/html");
  ec.println(F("<!DOCTYPE html>"));
  ec.println(F("<html>"));
  ec.println(F("<head>"));
  ec.println(F("<meta name=\"viewport\" content=\"width=500, initial-scale=1\">"));
  ec.println(F("<meta charset=\"utf-8\"/>"));
  ec.println(F("<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/xcontrol.css\">"));
  ec.println(F("<link rel=\"stylesheet\" type=\"text/css\" href=\"//fonts.googleapis.com/css?family=Roboto&amp;subset=latin,latin-ext\">"));
  ec.println(F("<script src=\"/js/common\"></script>"));
  ec.println(F("<script src=\"/js/custom\"></script>"));
  ec.print(F("<title>"));
  ec.print(XCONTROL_NAME);
  ec.println(F("</title>"));
  ec.println(F("</head>"));
  ec.println(F("<body>"));
}

// html footer and ending
void htmlFooter(EthernetClient ec) {
  ec.println(F("</body>"));
  ec.println(F("</html>"));
}

// html menu
void htmlMenu(EthernetClient ec) {
  ec.println(F("<menu>"));
  for (byte i = 0; i < menuCount; i++) {
    ec.print(F("<a href=\""));
    ec.print(menuLinks[i]);
    ec.print(F("\">"));
    ec.print(menuNames[i]);
    ec.println(F("</a>"));
  }
  ec.println(F("</menu>"));
  ec.println(F("<subm>"));
  ec.println(F("<a href=\"#\">"));
  ec.print(XCONTROL_NAME);
  ec.println(F("</a>"));
  ec.println(F("<a class=\"refresh\" href=\"/\">&olarr;</a>"));
  ec.println(F("</subm>"));
}

// css file
void htmlCSS(EthernetClient ec) {
  httpHeader(ec, "text/css");
  ec.println(F("* {font-family:Roboto,Helvetica,Arial,sans-serif;font-weight:normal;margin:0;padding:0;}"));
  ec.println(F("body {background-color:#eee;}"));
  ec.println(F("menu {width:100%;height:80px;background-color:#3f51b5;text-align:center;}"));
  ec.println(F("menu a {display:inline-block;color:white;line-height:40px;text-decoration:none;margin:0 20px;}"));
  ec.println(F("menu a:hover {border-bottom:solid 3px white;line-height:34px;}"));
  ec.println(F("subm {padding:0 20px;height:80px;background-color:#9fa8da;margin-top:-40px !important;margin-bottom:30px !important;}"));
  ec.println(F("subm a {display:inline-block;height:40px;line-height:40px;color:white;text-decoration:none;margin-right:20px;}"));
  ec.println(F("subm a.refresh {float:right;color:white;background-color:#3f51b5;width:40px;height:40px;line-height:40px;margin-top:59px;margin-right:0;text-align:center;border-radius:50%;}"));
  ec.println(F("card,subm {display:block;width:100%;max-width:500px;margin:10px auto;font-size:1.2em;border-radius:2px;box-shadow:0 10px 36px 0 rgba(0,0,0,0.2);}"));
  ec.println(F("card {padding:20px;background-color:white;color:#3f51b5;}"));
  ec.println(F("name {display:inline-block;width:100%;}"));
  ec.println(F("desc {display:inline-block;width:100%;font-size:0.8em;color:black;margin-top:10px;}"));
  ec.println(F("action {display:block;width:100%;height:36px;margin-top:20px;font-size:0.8em;line-height:40px;color:#c0c0c0;cursor:pointer;}"));
  ec.println(F("state,select {display:inline-block;font-size:0.8em;height:36px;line-height:36px;margin-left:20px;float:right;border-radius:2px;}"));
  ec.println(F("state {padding:0 20px;text-align:center;color:#9fa8da;background-color:white;transition:box-shadow 0.3s cubic-bezier(0.4,0,0.2,1)}"));
  ec.println(F("state:hover {box-shadow: 0px 4px 8px 0px rgba(0,0,0,0.4);}"));
  ec.println(F("state.active {color:white;background-color:#9fa8da;}"));
}

// js file
void htmlJSCommon(EthernetClient ec) {
  httpHeader(ec, "application/javascript");

  ec.println(F("function xcJSON(url, hfunc) {"));
  ec.println(F("  var r = new XMLHttpRequest();"));
  ec.println(F("  r.open('GET', url, true);"));
  ec.println(F("  r.onload = function() {"));
  ec.println(F("    if (r.status >= 200 && r.status < 400) {"));
  ec.println(F("      hfunc();"));
  ec.println(F("    }"));
  ec.println(F("  };"));
  ec.println(F("  r.send();"));
  ec.println(F("}\n"));

  ec.println(F("function xcAddClass(el, cn) {"));
  ec.println(F("  if (el.classList) el.classList.add(cn); else el.className += ' ' + cn;"));
  ec.println(F("}\n"));

  ec.println(F("function xcRemoveClass(el, cn) {"));
  ec.println(F("  if (el.classList) el.classList.remove(cn); else el.className = el.className.replace(new RegExp('(^|\\b)' + cn.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');"));
  ec.println(F("}\n"));

  ec.println(F("function xcFind(sel) {"));
  ec.println(F("  return document.querySelector(sel);"));
  ec.println(F("}\n"));
}

// json response
void jsonResponse(EthernetClient ec, byte id, String stat) {
  httpHeader(ec, "application/json");
  ec.print(F("{\"status\":\""));
  ec.print(stat);
  ec.println(F("\",\"id\":\""));
  ec.print(id);
  ec.println(F("\"}"));
}
